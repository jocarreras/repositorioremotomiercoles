import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in= new Scanner(System.in);
		char estadoCivil, nivelEstudios;
		int plus=0;
		 
		do{
			
			System.out.println("Estado civil s-soltero, c-casado, v-viudo, d-divorciado ");
			estadoCivil=in.nextLine().charAt(0);
			if (estadoCivil!='s' && estadoCivil!='c' &&
					estadoCivil!='v' && estadoCivil!='d' ){
				System.out.println("Error, el valor no es correcto..");
			}
			
		}while (estadoCivil!='s' && estadoCivil!='c' &&
				estadoCivil!='v' && estadoCivil!='d' );
		
		// while (exista un error){..}
		// nivelEstudios='h';  para que pueda entrar en el bucle
		System.out.println("Nivel de estudios p-primarios, m-medios, s-secundarios");
		nivelEstudios=in.nextLine().charAt(0);
		while(nivelEstudios!='p'&& nivelEstudios!='m' && nivelEstudios!='s'){
			System.out.println("Nivel de estudios p-primarios, m-medios, s-secundarios");
			nivelEstudios=in.nextLine().charAt(0);
			if (nivelEstudios!='p'&& nivelEstudios!='m' && nivelEstudios!='s'){
				System.out.println("Error, nivel de estudios no v�lido");
			}
		}
		
		switch(estadoCivil){
			case 's': 
					plus=6;
					break;
			case 'v': 
				    plus=(nivelEstudios=='p' || nivelEstudios=='s')?6:9;
	  			    break;
				
			case 'c':
					if (nivelEstudios=='p') plus=12;
					if (nivelEstudios=='m') plus=15;
					if (nivelEstudios=='s') plus=18;
					break;
			
			case 'd':
					if (nivelEstudios=='p') plus=12;
					if (nivelEstudios=='m') plus=14;
					if (nivelEstudios=='s') plus=16;
					break;
	
		}
		System.out.println("El plus es de " + plus +" �");

	}

}
