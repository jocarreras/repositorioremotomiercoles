import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
	Scanner in= new Scanner (System.in);
	int n;
	do{
	System.out.println("Tabla del numero");
	n=in.nextInt();
	if(n<=0){
		System.out.println("Error");
	}
	}while(n<=0);

	System.out.println("***TABLA DEL " + n + "**");
	for (int i=1; i<=10; i++){
		System.out.println(n + "*" + i + " = " + (n*i));
	}
	}

}
