import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner (System.in);
		
		int n1, n2, menor, mayor, suma=0;
		
		//1.Leer los dos valores
		System.out.println("N1 = ");
		n1=in.nextInt();
		System.out.println("N2 = ");
		n2=in.nextInt();
		
		//2. Calcular el menor y el mayor
		menor=Math.min(n1, n2);
		mayor=Math.max(n1, n2);
		
		//3. Calcular la suma y visualizarla
		for(int i=menor; i<=mayor; i++){
			if(i%2==0){
				suma=suma+i;
			}
			
		}
		System.out.println("La suma es " + suma);
	}

}
